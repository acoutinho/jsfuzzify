# jsfuzzify

jsfuzzify is a simple JavaScript library to fuzzify variables.
It's based on [jsfuzz][jsfuzz_link] from the user [sebs][sebs_link] in github.


## What you can do with this
Only __one simple thing__: convert a number in an expression in natural language, like converting the pressure of 100 to a "High" pressure. It gives the fuzzy set with higher belonging. Nothing more.


## How to use

Please see test.html file.

    :::javascript
    //1 - an "instance" of fuzzify is required for each different fuzzification rules
    var var1=new fuzzify();

    //2 - now we need to set the groups
    var1.addRange("Small", var1.triangle, [0,0,50]);
    var1.addRange("Medium", var1.triangle, [0,55,100]);
    var1.addRange("Large", var1.trapezoid, [65,100,300,300]);

    //3 - fuzzify
    var result1=var1.fuzzify(11);
    console.log(result1);

    //we can fuzzify several values using the same rules
    var result2=var1.fuzzify(85);
    console.log(result2);


### addRange parameters
1. The result, natural language expression
2. The type of figure for the fuzzy logic. This is the name of a function. Possibilities:
    * grade
    * reverseGrade
    * triangle
    * trapezoid
3. Options for the function. See the source and read the doc for the required values

## Authors
This was developed for an university assignment of the discipline Knowledge Based Systems for the Master in Computer Science at [Insitituto Superior de Engenharia do Porto][isep_en] in 2012, by [__António Coutinho__][acoutinho_bitbucket] and __Pedro Seixas__.

Based on the work of [**sebs** at github][sebs_link].

[jsfuzz_link]: https://github.com/sebs/jsfuzz
[sebs_link]: https://github.com/sebs
[isep_en]: http://www.isep.pt/index.php?sess=1
[acoutinho_bitbucket]: https://bitbucket.org/acoutinho
