/*
 * This project is based on jsfuzz from the user sebs in github https://github.com/sebs/jsfuzz
 */

// main object
var fuzzify = function() {
    this.ranges=[];
};

/**
 * Grade Type
 * @param val Value to check on
 * @param x0 left/lower side
 * @param x1 right upper side
 * @return
 */
fuzzify.prototype.grade = function (val, x0, x1) {
    var result = 0;
    var x = val;

    if (x <= x0) {
        result = 0;
    } else if (x >= x1) {
        result = 1;
    } else {
        result = (x/(x1-x0))-(x0/(x1-x0));
    }
    return result;
}

/**
 * Reverse Grade
 * @param val value to check on
 * @param x0 left/upper side
 * @param x1 right/lower side
 * @return
 */
fuzzify.prototype.reverseGrade = function(val, x0, x1) {
    var result = 0;
    var x = val;

    if (x <= x0) {
        result = 1;
    } else if (x >= x1) {
        result = 0;
    } else {
        result = (-x/(x1-x0))-(x1/(x1-x0));
    }
    return result;
}
/**
 * Triangle fuzzifyfication
 * @param val Value to check on
 * @param x0 left start of triangle
 * @param x1 top
 * @param x2 right side
 * @return
 */
fuzzify.prototype.triangle = function(val, x0, x1, x2) {
    var result = 0;
    var x = val;

    if (x <= x0) {
        result = 0;
    } else if (x == x1) {
        result = 1;
    } else if ((x>x0) && (x<x1)) {
        result = (x/(x1-x0))-(x0/(x1-x0));
    } else {
        result = (-x/(x2-x1))+(x2/(x2-x1));
    }
    return result;
}
/**
 * trapezoid fuzzifyfication
 * @param val Value to check on
 * @param x0 left start of the trapezoid
 * @param x1 upper left
 * @param x2 upper right
 * @param x3 lower rigth side
 * @return
 */
fuzzify.prototype.trapezoid = function(val, x0, x1, x2, x3) {
    var result = 0;
    var x = val;

    if (x < 0) {
        result = 0;
    } else if ((x>=x1) && (x<=x2)) {
        result = 1;
    } else if ((x>x0) && (x<x1)) {
        result = (x/(x1-x0))-(x0/(x1-x0))
    } else {
        result = (-x/(x3-x2))+(x3/(x3-x2));
    }
    return result;
}


/**
 * Add e new range to be considered.
 * @param value The result in natural language
 * @param func The shape. Possible values are this.trapezoid, this.triangle, this.reverseGrade, this.grade (this is an instance of this class)
 * @param params The parameters required by the function func
 */
fuzzify.prototype.addRange=function(value, func, params){
    this.ranges[this.ranges.length]=[value, func, params];
}

/**
 * Fuzzify the value
 * @param value The value to be fuzzifyfied
 * @return The result
 */
fuzzify.prototype.fuzzify=function(value){

    var resultFuzzy='';
    var resultFuzzyVal=0;

    for(var index in this.ranges){
        var range=this.ranges[index];

        var name=range[0];
        var func=range[1];
        var args=range[2];

        args=args.reverse();
        args.push(value);
        args=args.reverse();

        func = (typeof func == "function") ? func : undefined;
        var result = func.apply(this, args || []);

        if(result>resultFuzzyVal){
            resultFuzzyVal=result
            resultFuzzy=name;
        }
    }

    return resultFuzzy;
}